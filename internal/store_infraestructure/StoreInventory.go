package store

import (
	"errors"
	"strconv"

	"github.com/tealeg/xlsx"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//readinventoriesFromExcel Read file excel
func readinventoriesFromExcel() ([]entity.Inventory, error) {
	excelFileName := "Inventory.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		return nil, errors.New("Error to read file exel: " + err.Error())
	}

	inventories := []entity.Inventory{}
	sheet := xlFile.Sheet["Hoja1"]

	for c, row := range sheet.Rows {
		//Omit header file
		if c >= 1 {
			productID := row.Cells[0].String()
			unit := row.Cells[1].String()
			initInventory := row.Cells[2].String()
			initQuantity := row.Cells[3].String()
			comments := row.Cells[4].String()

			//product Id can't be empty
			if productID == "" {
				return nil, errors.New("The Id Product can't be empty")
			}

			//get initInventory, it must be type boolean
			initInventoryBool, err := strconv.ParseBool(initInventory)
			if err != nil {
				return nil, errors.New("Type InitInventory must be value true or false (" + initInventory + ")" + err.Error())
			}
			initQuantityInt := 0
			if initQuantity != "" {
				initQuantityInt, err = strconv.Atoi(initQuantity)
				if err != nil {
					return nil, errors.New("Type Init Quantity musb be type int" + err.Error())
				}
			}

			inventory := entity.Inventory{
				ProductID:     productID,
				Unit:          unit,
				InitInventory: initInventoryBool,
				InitQuantity:  initQuantityInt,
				Comments:      comments,
			}
			inventories = append(inventories, inventory)
		}
	}

	return inventories, nil
}

//InsertInventories Create many inventories in the database
func (st Store) InsertInventories() error {
	inventories, err := readinventoriesFromExcel()
	if err != nil {
		return errors.New("Error to insert many inventories: " + err.Error())
	}

	tx := st.Store.Begin()

	if tx.HasTable(&entity.Inventory{}) {
		for _, inv := range inventories {
			if err := tx.Create(&inv).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}
