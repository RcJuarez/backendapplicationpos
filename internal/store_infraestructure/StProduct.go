package store

import (
	"errors"
	"strconv"

	"github.com/tealeg/xlsx"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadProductsFromExcel Read file excel
func readProductsFromExcel() ([]entity.Product, error) {
	excelFileName := "products.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		return nil, errors.New("Error to read file exel: " + err.Error())
	}

	products := []entity.Product{}
	sheet := xlFile.Sheet["Hoja1"]

	for c, row := range sheet.Rows {
		//Omit header file
		if c >= 1 {
			code := row.Cells[0].String()
			barcode := row.Cells[1].String()
			name := row.Cells[2].String()
			description := row.Cells[3].String()
			commonNames := row.Cells[4].String()
			//get id Category, it must be type int
			catInt, err := strconv.Atoi(row.Cells[5].String())
			if err != nil {
				return nil, errors.New("Column category must be type Integer: (" + row.Cells[5].String() + ")" + err.Error())
			}

			product := entity.Product{

				Code:        code,
				BarCode:     barcode,
				Name:        name,
				Description: description,
				CommonNames: commonNames,
				CategoryID:  catInt,
			}
			products = append(products, product)
		}
	}

	return products, nil
}

//InsertProducts Create many products in the database
func (st Store) InsertProducts() error {
	products, err := readProductsFromExcel()
	if err != nil {
		return errors.New("Error to insert many products: " + err.Error())
	}

	tx := st.Store.Begin()

	if tx.HasTable(&entity.Product{}) {
		for _, prod := range products {
			if err := tx.Create(&prod).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}

//InsertProduct Create a product on the database
func (st Store) InsertProduct(product entity.Product) (*entity.Product, error) {
	if err := st.Store.Model(&entity.Product{}).Create(&product).Error; err != nil {
		return nil, err
	}
	return &product, nil
}

//SelectProducts Select all products on the database
func (st Store) SelectProducts() (*[]entity.Product, error) {
	prod := &[]entity.Product{}
	st.Store.Find(prod)
	return prod, nil
}

//SelectProduct Return a product with a code pased en the parameter
func (st Store) SelectProduct(product entity.Product) (*entity.Product, error) {
	prod := &entity.Product{}
	err := st.Store.First(prod, "code = ?", product.Code).Error
	return prod, err
}

//SelectProductByCode Return a product with a code pased en the parameter
func (st Store) SelectProductByCode(searched string) ([]entity.Product, error) {
	prod := []entity.Product{}
	err := st.Store.Where("code LIKE ?", "%"+searched+"%").Or("name LIKE ?", "%"+searched+"%").Or("common_names like ?", "%"+searched+"%").Find(&prod).Error
	return prod, err
}
