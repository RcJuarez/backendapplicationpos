package store

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"encoding/csv"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadSalesFromCSV read a file csv and get all sales
func (st Store) ReadSalesFromCSV() ([]entity.Sale, error) {
	csvFile, ferr := os.Open("Sales.csv")
	if ferr != nil {
		log.Fatal(ferr)
	}
	r := csv.NewReader(csvFile)

	sales := []entity.Sale{}
	readHeader := 0
	for {

		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if readHeader >= 1 {
			id := record[0]
			dateCreate := record[1]
			codeProd := record[2]
			quantity := record[3]
			price := record[4]
			amount := record[5]
			sellerID := record[6]
			customerID := record[7]
			comments := record[8]

			if id == "" {
				return nil, errors.New("Column id can't be empty")
			}
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("Id must be type Int like (15) you have: (" + id + ")")
			}
			if dateCreate == "" {
				return nil, errors.New("Column date create can't be empty")
			}
			layout := "2006-01-02"
			dateCreateDate, err := time.Parse(layout, dateCreate)
			if err != nil {
				return nil, errors.New("Column date must be type Date as: " + layout + " . You have: (" + dateCreate + ") " + err.Error())
			}

			if codeProd == "" {
				return nil, errors.New("Column code product can't be empty")
			}
			if quantity == "" {
				return nil, errors.New("Column quantity can't be empty")
			}
			quantityFloat64, err := strconv.ParseFloat(quantity, 64)
			if err != nil {
				return nil, errors.New("Quantity must be type Float like 5.00")
			}
			if price == "" {
				return nil, errors.New("Column price can't be empty")
			}
			priceFloat64, err := strconv.ParseFloat(price, 64)
			if err != nil {
				return nil, errors.New("Price must be type Float like 5.00")
			}
			if amount == "" {
				return nil, errors.New("Column account can't be empty")
			}
			amountFloat64, err := strconv.ParseFloat(amount, 64)
			if err != nil {
				return nil, errors.New("Amount must be type Float like 5.00")
			}

			sellerIDInt, err := strconv.Atoi(sellerID)
			if err != nil {
				return nil, errors.New("sellerIDInt must be type Int like (15) you have: (" + id + ")")
			}

			customerIDInt, err := strconv.Atoi(customerID)
			if err != nil {
				return nil, errors.New("customerIDInt must be type Int like (15) you have: (" + id + ")")
			}

			sale := entity.Sale{
				ID:         idInt,
				CreatedAt:  dateCreateDate,
				ProductID:  codeProd,
				Quantity:   quantityFloat64,
				Price:      priceFloat64,
				Ammount:    amountFloat64,
				SellerID:   sellerIDInt,
				CustomerID: customerIDInt,
				Comments:   comments,
			}

			sales = append(sales, sale)
		}
		readHeader++
	}
	return sales, nil
}

//InsertSales Create many sales in the database
func (st Store) InsertSales(sales []entity.Sale) error {

	tx := st.Store.Begin()

	if tx.HasTable(&entity.Sale{}) {
		for _, sal := range sales {
			if err := tx.Create(&sal).Error; err != nil {
				fmt.Println(sal)
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}

//InsertSale create one sale in the database
func (st Store) InsertSale(data entity.Sale) error {
	return st.Store.Create(&data).Error
}

//SelectSameSale Return a sale with supplier id and saluct id and Sale
func (st Store) SelectSameSale(sale entity.Sale) (entity.Sale, error) {
	dataStruct := entity.Sale{}
	err := st.Store.Where(&entity.Sale{CreatedAt: sale.CreatedAt, ProductID: sale.ProductID, Price: sale.Price}).First(&dataStruct).Error
	if err != nil {
		if err.Error() == "record not found" {
			return dataStruct, nil
		}
	}
	return dataStruct, err
}
