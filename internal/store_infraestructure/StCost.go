package store

import (
	"encoding/csv"
	"errors"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadCostsFromCSV read cost from file CSV and return all cost founded
func (st Store) ReadCostsFromCSV() ([]entity.Cost, error) {
	csvFile, ferr := os.Open("Costs.csv")
	if ferr != nil {
		log.Fatal(ferr)
	}
	r := csv.NewReader(csvFile)

	costs := []entity.Cost{}
	readHeader := 0
	for {

		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if readHeader >= 1 {
			idSupplier := record[0]
			codeProd := record[1]
			valueCost := record[2]
			dateCreate := record[3]

			if dateCreate == "" {
				return nil, errors.New("Column date create can't be empty")
			}
			layout := "2006-01-02"
			dateCreateDate, err := time.Parse(layout, dateCreate)
			if err != nil {
				return nil, errors.New("Column date must be type Date as: " + layout + " . You have: (" + dateCreate + ") " + err.Error())
			}

			idSupplierInt, err := strconv.Atoi(idSupplier)
			if err != nil {
				return nil, errors.New("Id Supplier must be type Int like 50")
			}
			if codeProd == "" {
				return nil, errors.New("Column id product can't be empty")
			}

			if valueCost == "" {
				return nil, errors.New("Column cost can't be empty")
			}
			costFloat64, err := strconv.ParseFloat(valueCost, 64)
			if err != nil {
				return nil, errors.New("Cost must be type Float like 5.00")
			}
			loc, _ := time.LoadLocation("America/Mexico_City")
			cost := entity.Cost{
				CreatedAt:  dateCreateDate.In(loc),
				SupplierID: idSupplierInt,
				ProductID:  codeProd,
				ValueCost:  costFloat64,
			}

			costs = append(costs, cost)
		}
		readHeader++
	}
	return costs, nil
}

//InsertCost Create a cost on the database
func (st Store) InsertCost(cost entity.Cost) error {
	if err := st.Store.Model(&entity.Cost{}).Create(&cost).Error; err != nil {
		return err
	}
	return nil
}

//SelectCostBySupplierAndProductAndCost Return a Cost with supplier id and product id and cost
func (st Store) SelectCostBySupplierAndProductAndCost(cost entity.Cost) (entity.Cost, error) {
	dataStruct := entity.Cost{}
	err := st.Store.Where(&entity.Cost{SupplierID: cost.SupplierID, ProductID: cost.ProductID, ValueCost: cost.ValueCost}).First(&dataStruct).Error
	if err != nil {
		if err.Error() == "record not found" {
			return dataStruct, nil
		}
	}
	return dataStruct, err
}

//UpdateCostDateCreate udate only field CreateAt
func (st Store) UpdateCostDateCreate(cost entity.Cost) error {
	return st.Store.Model(&cost).Update("CreatedAt", cost.CreatedAt).Error
}

//SelectLastCost return the last cost order by date recently where idsupplier and id product
func (st Store) SelectLastCost(cost entity.Cost) (entity.Cost, error) {

	var costFound entity.Cost
	err := st.Store.Order("created_at desc").First(&costFound, &entity.Cost{SupplierID: cost.SupplierID, ProductID: cost.ProductID}).Error
	return costFound, err
}
