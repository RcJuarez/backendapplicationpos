package store

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	"encoding/csv"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadPriceListsFromCSV read a file csv and get all priceLists
func (st Store) ReadPriceListsFromCSV() ([]entity.PriceList, error) {
	csvFile, ferr := os.Open("Resources/PriceLists.csv")
	if ferr != nil {
		log.Fatal(ferr)
	}
	r := csv.NewReader(csvFile)

	priceLists := []entity.PriceList{}
	readHeader := 0
	for {

		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if readHeader >= 1 {

			id := record[0]
			codeProd := record[1]
			price := record[2]

			if id == "" {
				return nil, errors.New("Column List id can't be empty")
			}
			nameListID, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("List Id must be type Int like (15) you have: (" + id + ")")
			}

			if codeProd == "" {
				return nil, errors.New("Column code product can't be empty")
			}

			if price == "" {
				return nil, errors.New("Column price can't be empty")
			}
			priceFloat64, err := strconv.ParseFloat(price, 64)
			if err != nil {
				return nil, fmt.Errorf("Price must be type FLoat like 5.00, you have: (%s)", price)
			}

			priceList := entity.PriceList{
				NameListID: nameListID,
				ProductID:  codeProd,
				Price:      priceFloat64,
			}

			priceLists = append(priceLists, priceList)
		}
		readHeader++
	}
	return priceLists, nil
}

//InsertPriceLists Create many priceLists in the database
func (st Store) InsertPriceLists(priceList []entity.PriceList) error {

	tx := st.Store.Begin()

	if tx.HasTable(&entity.PriceList{}) {
		for _, data := range priceList {
			if err := tx.Create(&data).Error; err != nil {
				fmt.Println(data)
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}

//InsertPriceList create one price into a price list in the database
func (st Store) InsertPriceList(priceList entity.PriceList) error {
	var data entity.PriceList
	return st.Store.Create(&data).Error
}
