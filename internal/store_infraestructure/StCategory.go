package store

import (
	"errors"
	"strconv"

	"github.com/tealeg/xlsx"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//readCategoriesFromExcel Read file excel
func readCategoriesFromExcel() ([]entity.Category, error) {
	excelFileName := "categories.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		return nil, errors.New("Error to read file exel: " + err.Error())
	}

	categories := []entity.Category{}
	sheet := xlFile.Sheet["Hoja1"]

	for c, row := range sheet.Rows {
		//Omit header file
		if c >= 1 {
			id := row.Cells[0].String()
			name := row.Cells[1].String()
			//get id, it must be type int
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("Id category must be type Integer: (" + row.Cells[5].String() + ")" + err.Error())
			}

			category := entity.Category{

				ID:   idInt,
				Name: name,
			}
			categories = append(categories, category)
		}
	}

	return categories, nil
}

//InsertCategory create a category on a database
func (st Store) InsertCategory(category entity.Category) (*entity.Category, error) {
	if err := st.Store.Model(&entity.Category{}).Create(&category).Error; err != nil {
		return nil, err
	}
	return &category, nil
}

//SelectCategories Find all categories on the database
func (st Store) SelectCategories() (*[]entity.Category, error) {
	categ := &[]entity.Category{}
	st.Store.Find(categ)
	return categ, nil
}
