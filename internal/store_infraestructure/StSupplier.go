package store

import (
	"errors"
	"strconv"

	"github.com/tealeg/xlsx"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadSuppliersFromExcel Read file excel
func readSuppliersFromExcel() ([]entity.Supplier, error) {
	excelFileName := "Suppliers.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		return nil, errors.New("Error to read file exel: " + err.Error())
	}

	suppliers := []entity.Supplier{}
	sheet := xlFile.Sheet["Hoja1"]

	//Number rows
	numberRows := 0
	for c, row := range sheet.Rows {
		//Valida lenght header
		if c == 0 {
			numberRows = len(row.Cells)
		}

		//Omit header file
		if c >= 1 {
			var id, fullname, address, phone, email string

			data := [6]string{id, fullname, address, phone, email}

			for k := len(row.Cells) - 1; k >= 0; k-- {
				if k <= numberRows {
					if k > numberRows {
						data[k] = ""
					} else {
						data[k] = row.Cells[k].String()
					}
				}
			}

			id = data[0]
			fullname = data[1]
			address = data[2]
			phone = data[3]
			email = data[4]
			//get id Category, it must be type int
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("Column id supplier must be type Integer: (" + id + ")" + err.Error())
			}

			product := entity.Supplier{

				ID:       idInt,
				Fullname: fullname,
				Addres:   address,
				Phone:    phone,
				Email:    email,
			}
			suppliers = append(suppliers, product)
		}
	}

	return suppliers, nil
}

//InsertSuppliers Create many suppliers in the database
func (st Store) InsertSuppliers() error {
	suppliers, err := readSuppliersFromExcel()
	if err != nil {
		return errors.New("Error to insert many suppliers: " + err.Error())
	}

	tx := st.Store.Begin()

	if tx.HasTable(&entity.Supplier{}) {
		for _, prod := range suppliers {
			if err := tx.Create(&prod).Error; err != nil {
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}
