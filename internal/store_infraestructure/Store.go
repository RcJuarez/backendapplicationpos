package store

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/config"
)

//Store is a struct with a pointer to database
type Store struct {
	Store *gorm.DB
}

//InitDatabase starting a instance of a database
func InitDatabase(configDataBase config.ConfigServicio) (Store, error) {
	if PsqlDB == nil {
		var err error
		dbConfig := fmt.Sprintf("host=%s user=%s password=%s port=%s dbname=%s sslmode=disable", configDataBase.Servidor, configDataBase.Usuario, configDataBase.Pass, configDataBase.Puerto, configDataBase.NombreDB)
		PsqlDB, err = gorm.Open("postgres", dbConfig)
		if err != nil {
			return GetDB(), err
		}
		//PsqlDB.SingularTable(true)
		PsqlDB.LogMode(true)
		PsqlDB.Debug()
	}
	return GetDB(), nil
}

//GetDB retrun  a pointer to a database
func GetDB() Store {
	return Store{
		Store: PsqlDB,
	}
}

//PsqlDB variable global de conexión a Base de Datos en Sql
var PsqlDB *gorm.DB

//InitTables create all tabless
func InitTables(dataBase Store) error {
	//Starting a transaction
	tx := dataBase.Store.Begin()

	//Create a Category Table
	if err := InitCategoryTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Product Table
	if err := InitProductTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Supplier Table
	if err := InitSupplierTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Supplier Table
	if err := InitCostListTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Cost Table
	if err := InitCostTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Customer Table
	if err := InitCustomerTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Inventory Table
	if err := InitInventoryTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a PriceList Table
	if err := InitNameListTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a PriceList Table
	if err := InitPriceListTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Purchase Table
	if err := InitPurchaseTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Seller Table
	if err := InitSellerTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	//Create a Sale Table
	if err := InitSaleTable(tx); err != nil {
		tx.Rollback()
		return err
	}

	if err := tx.Commit().Error; err != nil {
		return err
	}

	return nil
}

//InitCategoryTable Create table category in a database
func InitCategoryTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Category{}) {
		if err := tx.CreateTable(&entity.Category{}).Error; err != nil {
			return err
		}

		err := tx.Create(
			&entity.Category{
				ID:   1,
				Name: "Papelería",
			},
		).Error
		if err != nil {
			return err
		}
	}
	return nil
}

//InitProductTable create a product table in a database
func InitProductTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Product{}) {
		if err := tx.CreateTable(&entity.Product{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.Product{}).AddForeignKey("category_id", "categories(ID)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitSupplierTable create a product table in a database
func InitSupplierTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Supplier{}) {
		if err := tx.CreateTable(&entity.Supplier{}).Error; err != nil {
			return err
		}
	}
	return nil
}

//InitCostListTable create a CostList table in a database
func InitCostListTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Cost{}) {
		if err := tx.CreateTable(&entity.Cost{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.Cost{}).AddForeignKey("supplier_id", "suppliers(id)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitCostTable create a Cost table in a database
func InitCostTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Cost{}) {
		if err := tx.CreateTable(&entity.Cost{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.Cost{}).AddForeignKey("supplier_id", "suppliers(id)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
		err2 = tx.Model(&entity.Cost{}).AddForeignKey("product_id", "products(id)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitCustomerTable create a Customer table in a database
func InitCustomerTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Customer{}) {
		if err := tx.CreateTable(&entity.Customer{}).Error; err != nil {
			return err
		}
	}
	return nil
}

//InitInventoryTable create a Inventory table in a database
func InitInventoryTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Inventory{}) {
		if err := tx.CreateTable(&entity.Inventory{}).Error; err != nil {
			return err
		}

		err2 := tx.Model(&entity.Inventory{}).AddForeignKey("product_id", "products(Code)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitNameListTable create a NameList table in a database
func InitNameListTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.NameList{}) {
		if err := tx.CreateTable(&entity.NameList{}).Error; err != nil {
			return err
		}
	}
	return nil
}

//InitPriceListTable create a PriceList table in a database
func InitPriceListTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.PriceList{}) {
		if err := tx.CreateTable(&entity.PriceList{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.PriceList{}).AddForeignKey("product_id", "products(Code)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitPurchaseTable create a Purchase table in a database
func InitPurchaseTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Purchase{}) {
		if err := tx.CreateTable(&entity.Purchase{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.Purchase{}).AddForeignKey("product_id", "products(Code)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}

//InitSellerTable create a Seller table in a database
func InitSellerTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Seller{}) {
		if err := tx.CreateTable(&entity.Seller{}).Error; err != nil {
			return err
		}
	}
	return nil
}

//InitSaleTable create a Sale table in a database
func InitSaleTable(tx *gorm.DB) error {
	if !tx.HasTable(&entity.Sale{}) {
		if err := tx.CreateTable(&entity.Sale{}).Error; err != nil {
			return err
		}
		err2 := tx.Model(&entity.Sale{}).AddForeignKey("product_id", "products(Code)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
		err2 = tx.Model(&entity.Sale{}).AddForeignKey("seller_id", "sellers(id)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
		err2 = tx.Model(&entity.Sale{}).AddForeignKey("customer_id", "customers(id)", "CASCADE", "CASCADE").Error
		if err2 != nil {
			return err2
		}
	}
	return nil
}
