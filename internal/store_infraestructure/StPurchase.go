package store

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"encoding/csv"

	"github.com/tealeg/xlsx"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ReadPurchasesFromExcel Read file excel
func readPurchasesFromExcel() ([]entity.Purchase, error) {
	excelFileName := "Purchases.xlsx"
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		return nil, errors.New("Error to read file exel: " + err.Error())
	}

	purchases := []entity.Purchase{}
	sheet := xlFile.Sheet["Hoja1"]

	for c, row := range sheet.Rows {
		//Omit header file
		if c >= 1 {
			id := row.Cells[0].String()
			layout := "2006-01-02T15:04:05.000Z"
			//layout := "01/02/2006"

			dateCreate := row.Cells[1].String()
			fmt.Println("row.Cells[1].String() ", row.Cells[1].GetNumberFormat())
			idSupplier := row.Cells[2].String()
			codeProd := row.Cells[3].String()
			quantity := row.Cells[4].String()
			cost := row.Cells[5].String()
			amount := row.Cells[6].String()
			comments := row.Cells[7].String()

			if id == "" {
				return nil, errors.New("Column id create can't be empty")
			}
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("Id must be type Int like 15")
			}
			if dateCreate == "" {
				return nil, errors.New("Column date create can't be empty")
			}

			dateCreateDate, err := time.Parse(layout, dateCreate)
			if err != nil {
				return nil, errors.New("Column date must be type Date as: " + layout + " . You have: (" + dateCreate + ") " + err.Error())
			}

			idSupplierInt, err := strconv.Atoi(idSupplier)
			if err != nil {
				return nil, errors.New("Id Supplier must be type Int like 50")
			}
			if codeProd == "" {
				return nil, errors.New("Column id product can't be empty")
			}
			if quantity == "" {
				return nil, errors.New("Column quantity can't be empty")
			}
			quantityFloat64, err := strconv.ParseFloat(quantity, 64)
			if err != nil {
				return nil, errors.New("Quantity must be type Float like 5.00")
			}
			if cost == "" {
				return nil, errors.New("Column cost can't be empty")
			}
			costFloat64, err := strconv.ParseFloat(cost, 64)
			if err != nil {
				return nil, errors.New("Cost must be type Float like 5.00")
			}
			if amount == "" {
				return nil, errors.New("Column account can't be empty")
			}
			amountFloat64, err := strconv.ParseFloat(amount, 64)
			if err != nil {
				return nil, errors.New("Amount must be type Float like 5.00")
			}

			purchase := entity.Purchase{
				ID:         idInt,
				CreatedAt:  dateCreateDate,
				SupplierID: idSupplierInt,
				ProductID:  codeProd,
				Quantity:   quantityFloat64,
				ValueCost:  costFloat64,
				Ammount:    amountFloat64,
				Comments:   comments,
			}
			purchases = append(purchases, purchase)
		}
	}

	return purchases, nil
}

func readPurchasesFromCSV() ([]entity.Purchase, error) {
	csvFile, ferr := os.Open("Purchases.csv")
	if ferr != nil {
		log.Fatal(ferr)
	}
	r := csv.NewReader(csvFile)

	purchases := []entity.Purchase{}
	readHeader := 0
	for {

		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if readHeader >= 1 {
			id := record[0]
			dateCreate := record[1]
			idSupplier := record[2]
			codeProd := record[3]
			quantity := record[4]
			cost := record[5]
			amount := record[6]
			comments := record[7]

			if id == "" {
				return nil, errors.New("Column id create can't be empty")
			}
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return nil, errors.New("Id must be type Int like (15) you have: (" + id + ")")
			}
			if dateCreate == "" {
				return nil, errors.New("Column date create can't be empty")
			}
			layout := "2006-01-02"
			dateCreateDate, err := time.Parse(layout, dateCreate)
			if err != nil {
				return nil, errors.New("Column date must be type Date as: " + layout + " . You have: (" + dateCreate + ") " + err.Error())
			}

			idSupplierInt, err := strconv.Atoi(idSupplier)
			if err != nil {
				return nil, errors.New("Id Supplier must be type Int like 50")
			}
			if codeProd == "" {
				return nil, errors.New("Column id product can't be empty")
			}
			if quantity == "" {
				return nil, errors.New("Column quantity can't be empty")
			}
			quantityFloat64, err := strconv.ParseFloat(quantity, 64)
			if err != nil {
				return nil, errors.New("Quantity must be type Float like 5.00")
			}
			if cost == "" {
				return nil, errors.New("Column cost can't be empty")
			}
			costFloat64, err := strconv.ParseFloat(cost, 64)
			if err != nil {
				return nil, errors.New("Cost must be type Float like 5.00")
			}
			if amount == "" {
				return nil, errors.New("Column account can't be empty")
			}
			amountFloat64, err := strconv.ParseFloat(amount, 64)
			if err != nil {
				return nil, errors.New("Amount must be type Float like 5.00")
			}

			purchase := entity.Purchase{
				ID:         idInt,
				CreatedAt:  dateCreateDate,
				SupplierID: idSupplierInt,
				ProductID:  codeProd,
				Quantity:   quantityFloat64,
				ValueCost:  costFloat64,
				Ammount:    amountFloat64,
				Comments:   comments,
			}

			purchases = append(purchases, purchase)
		}
		readHeader++
	}
	return purchases, nil
}

//InsertPurchases Create many purchases in the database
func (st Store) InsertPurchases() error {
	purchases, err := readPurchasesFromCSV()
	if err != nil {
		return errors.New("Error to insert many purchases: " + err.Error())
	}

	tx := st.Store.Begin()

	if tx.HasTable(&entity.Purchase{}) {
		for _, prod := range purchases {
			if err := tx.Create(&prod).Error; err != nil {
				fmt.Println(prod)
				tx.Rollback()
				return err
			}
		}
		if err := tx.Commit().Error; err != nil {
			return err
		}
	}
	return nil
}

//InsertPurchase create one purchase in the database
func (st Store) InsertPurchase(data entity.Purchase) error {
	return st.Store.Create(&data).Error
}

//SelectPurchaseBySupplierAndProductAndCost Return a purchase with supplier id and product id and Purchase
func (st Store) SelectPurchaseBySupplierAndProductAndCost(purchase entity.Purchase) (entity.Purchase, error) {
	dataStruct := entity.Purchase{}
	err := st.Store.Where(&entity.Purchase{CreatedAt: purchase.CreatedAt, SupplierID: purchase.SupplierID, ProductID: purchase.ProductID, ValueCost: purchase.ValueCost}).First(&dataStruct).Error
	if err != nil {
		if err.Error() == "record not found" {
			return dataStruct, nil
		}
	}
	return dataStruct, err
}
