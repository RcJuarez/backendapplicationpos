package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//PurchaseUseCase methods CRUD that need store or infraestructure
type PurchaseUseCase interface {
	CreatePurchases() error
	CreatePurchase(purchase entity.Purchase) error
	//FindPurchases() (*[]entity.Purchase, error)
	//FindPurchase(purchase entity.Purchase) (*entity.Purchase, error)
}

//PurchaseMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type PurchaseMethodUC struct {
	UseCase PurchaseUseCase
}

//NewInterfaceUseCasePurchase return an interface produt of use case
func NewInterfaceUseCasePurchase(purchaseUseCase PurchaseUseCase) PurchaseMethodUC {
	return PurchaseMethodUC{
		UseCase: purchaseUseCase,
	}
}

//WSCreatePurchases Web service many Purchases in a database
func (prod PurchaseMethodUC) WSCreatePurchases(c echo.Context) error {

	err := prod.UseCase.CreatePurchases()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all purchases")
	}

	return c.JSON(http.StatusCreated, "Purchases create succesfuly")
}

//WSCreatePurchase Web service a Purchase in a database
func (prod PurchaseMethodUC) WSCreatePurchase(c echo.Context) error {

	purchaseBody := entity.Purchase{}

	if err := c.Bind(&purchaseBody); err != nil {
		return errors.WrapError(err, 400, "Purchase body request not found")
	}

	err := prod.UseCase.CreatePurchase(purchaseBody)
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all purchases")
	}

	return c.JSON(http.StatusCreated, "Purchases create succesfuly")
}
