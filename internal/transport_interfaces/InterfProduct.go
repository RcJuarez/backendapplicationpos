package transport

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//ProductUseCase methods CRUD that need store or infraestructure
type ProductUseCase interface {
	CreateProducts() error
	CreateProduct(product entity.Product) (*entity.Product, error)
	FindProducts() (*[]entity.Product, error)
	FindProduct(product entity.Product) (*entity.Product, error)
	SelectProductByCode(searched string) ([]entity.Product, error)
}

//ProductMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type ProductMethodUC struct {
	UseCase ProductUseCase
}

//NewInterfaceUseCaseProduct return an interface produt of use case
func NewInterfaceUseCaseProduct(productUseCase ProductUseCase) ProductMethodUC {
	return ProductMethodUC{
		UseCase: productUseCase,
	}
}

//WSCreateProducts Web service many Products in a database
func (prod ProductMethodUC) WSCreateProducts(c echo.Context) error {

	err := prod.UseCase.CreateProducts()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all products")
	}

	return c.JSON(http.StatusCreated, "Products create succesfuly")
}

//WSCreateProduct Web service that create a product
func (prod ProductMethodUC) WSCreateProduct(c echo.Context) error {
	productBody := entity.Product{}
	if err := c.Bind(&productBody); err != nil {
		return errors.WrapError(err, 400, "El body request es inválido")
	}

	productCreated, err := prod.UseCase.CreateProduct(productBody)
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert a product")
	}

	return c.JSON(http.StatusCreated, productCreated)
}

//WSViewProducts WebService that return all produts of database
func (prod ProductMethodUC) WSViewProducts(context echo.Context) error {
	prods, err := prod.UseCase.FindProducts()
	if err != nil {
		return errors.WrapError(err, http.StatusInternalServerError, "Error to show products")
	}
	return context.JSON(http.StatusOK, prods)
}

//WSViewManyProducts WebService that return many products of a database
func (prod ProductMethodUC) WSViewManyProducts(context echo.Context) error {
	fmt.Println("xxxxxxxxxxxxxxxxxxxxxxxxxx")
	cod := context.QueryParam("cadena")
	//cod := context.Param("searchQuery")
	if cod == "" {
		return errors.WrapError(nil, http.StatusBadRequest, "code can't be empty")
	}
	fmt.Println("Code searched: ", cod)
	product, err := prod.UseCase.SelectProductByCode(cod)

	if err != nil {
		return errors.WrapError(err, http.StatusInternalServerError, "Error finding product")
	}

	return context.JSON(http.StatusOK, product)
}

//WSViewOneProduct WebService that return all products of a database
func (prod ProductMethodUC) WSViewOneProduct(context echo.Context) error {
	cod := context.Param("id")
	if cod == "" {
		return errors.WrapError(nil, http.StatusBadRequest, "Id can't be empty")
	}

	product, err := prod.UseCase.FindProduct(entity.Product{Code: cod})

	if err != nil {
		return errors.WrapError(err, http.StatusInternalServerError, "Error finding product")
	}

	return context.JSON(http.StatusOK, product)
}
