package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//PriceListUseCase methods CRUD that need store or infraestructure
type PriceListUseCase interface {
	CreatePriceLists() error
	CreatePriceList(priceList entity.PriceList) error
	//FindPriceLists() (*[]entity.PriceList, error)
	//FindPriceList(priceList entity.PriceList) (*entity.PriceList, error)
}

//PriceListMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type PriceListMethodUC struct {
	UseCase PriceListUseCase
}

//NewInterfaceUseCasePriceList return an interface produt of use case
func NewInterfaceUseCasePriceList(priceListUseCase PriceListUseCase) PriceListMethodUC {
	return PriceListMethodUC{
		UseCase: priceListUseCase,
	}
}

//WSCreatePriceLists Web service many PriceLists in a database
func (prod PriceListMethodUC) WSCreatePriceLists(c echo.Context) error {

	err := prod.UseCase.CreatePriceLists()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all priceLists")
	}

	return c.JSON(http.StatusCreated, "PriceLists create succesfuly")
}

//WSCreatePriceList Web service a PriceList in a database
func (prod PriceListMethodUC) WSCreatePriceList(c echo.Context) error {

	priceListBody := entity.PriceList{}

	if err := c.Bind(&priceListBody); err != nil {
		return errors.WrapError(err, 400, "PriceList body request not found")
	}

	err := prod.UseCase.CreatePriceList(priceListBody)
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all priceLists")
	}

	return c.JSON(http.StatusCreated, "PriceLists create succesfuly")
}
