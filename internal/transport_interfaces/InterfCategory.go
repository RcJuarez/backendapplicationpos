package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//CategoryUseCase methods CRUD that need store or infraestructure
type CategoryUseCase interface {
	CreateCategory(category entity.Category) (*entity.Category, error)
	FindCategories() (*[]entity.Category, error)
}

//CategoryMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type CategoryMethodUC struct {
	UseCase CategoryUseCase
}

//NewInterfaceUseCaseCategory return an interface that contains insert and select categories
func NewInterfaceUseCaseCategory(CategoryUseCase CategoryUseCase) CategoryMethodUC {
	return CategoryMethodUC{
		UseCase: CategoryUseCase,
	}
}

//WSCreateCategory Web service that create a category
func (cat CategoryMethodUC) WSCreateCategory(c echo.Context) error {
	CategoryBody := entity.Category{}
	if err := c.Bind(&CategoryBody); err != nil {
		return errors.WrapError(err, 400, "El body request es inválido")
	}

	CategoryCreated, err := cat.UseCase.CreateCategory(CategoryBody)
	if err != nil {
		return errors.WrapError(err, http.StatusInternalServerError, "Error to create category")
	}

	return c.JSON(http.StatusCreated, CategoryCreated)
}

//WSViewCategories WebService that return all categories of database
func (cat CategoryMethodUC) WSViewCategories(c echo.Context) error {
	categories, err := cat.UseCase.FindCategories()
	if err != nil {
		return errors.WrapError(err, 500, "Error to fetching categories")
	}
	return c.JSON(http.StatusOK, categories)
}
