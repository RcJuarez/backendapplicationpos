package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//InventoryUseCase methods CRUD that need store or infraestructure
type InventoryUseCase interface {
	CreateInventories() error
}

//InventoryMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type InventoryMethodUC struct {
	UseCase InventoryUseCase
}

//NewInterfaceUseCaseInventory return an interface inventory of use case
func NewInterfaceUseCaseInventory(inventoryUseCase InventoryUseCase) InventoryMethodUC {
	return InventoryMethodUC{
		UseCase: inventoryUseCase,
	}
}

//WSCreateInventories Web service many Invntory in a database
func (inv InventoryMethodUC) WSCreateInventories(c echo.Context) error {

	err := inv.UseCase.CreateInventories()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all inventories")
	}

	return c.JSON(http.StatusCreated, "Inventories create succesfuly")
}
