package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//SupplierUseCase methods CRUD that need store or infraestructure
type SupplierUseCase interface {
	CreateSuppliers() error
	//CreateSupplier(supplier entity.Supplier) (*entity.Supplier, error)
	//FindSuppliers() (*[]entity.Supplier, error)
	//FindSupplier(supplier entity.Supplier) (*entity.Supplier, error)
}

//SupplierMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type SupplierMethodUC struct {
	UseCase SupplierUseCase
}

//NewInterfaceUseCaseSupplier return an interface produt of use case
func NewInterfaceUseCaseSupplier(supplierUseCase SupplierUseCase) SupplierMethodUC {
	return SupplierMethodUC{
		UseCase: supplierUseCase,
	}
}

//WSCreateSuppliers Web service many Suppliers in a database
func (prod SupplierMethodUC) WSCreateSuppliers(c echo.Context) error {

	err := prod.UseCase.CreateSuppliers()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all suppliers")
	}

	return c.JSON(http.StatusCreated, "Suppliers create succesfuly")
}
