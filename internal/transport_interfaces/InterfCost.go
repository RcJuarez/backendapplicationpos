package transport

import (
	"net/http"
	"strconv"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"

	"github.com/labstack/echo/v4"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//CostUseCase methods CRUD that need store or infraestructure
type CostUseCase interface {
	CreateCosts() error
	GetLastCost(cost entity.Cost) (entity.Cost, error)
}

//CostMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type CostMethodUC struct {
	UseCase CostUseCase
}

//NewInterfaceUseCaseCost return an interface produt of use case
func NewInterfaceUseCaseCost(costUseCase CostUseCase) CostMethodUC {
	return CostMethodUC{
		UseCase: costUseCase,
	}
}

//WSCreateCosts Web service many Costs in a database
func (accUC CostMethodUC) WSCreateCosts(c echo.Context) error {
	err := accUC.UseCase.CreateCosts()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all costs")
	}

	return c.JSON(http.StatusCreated, "Costs create succesfuly")
}

//WSGetLastCost Web service that getting the last cost order by created at, require id product and id supplier
func (accUC CostMethodUC) WSGetLastCost(c echo.Context) error {
	productID := c.FormValue("productId")
	supplierID := c.FormValue("supplierId")

	if productID == "" || supplierID == "" {
		return errors.WrapError(nil, 500, "Product Id or supplier id can't be empty")
	}

	supplierIDInt, err := strconv.Atoi(supplierID)
	if err != nil {
		return errors.WrapError(err, http.StatusInternalServerError, "Error converting supplier Id")
	}

	cost, err := accUC.UseCase.GetLastCost(entity.Cost{SupplierID: supplierIDInt, ProductID: productID})
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all costs")
	}

	return c.JSON(http.StatusCreated, cost)
}
