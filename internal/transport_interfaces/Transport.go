package transport

import "github.com/labstack/echo/v4"

//RouterCategory Method rest for categories
func RouterCategory(useCase CategoryUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseCategory(useCase)

	groupRoute := e.Group("/categories")
	groupRoute.GET("", transp.WSViewCategories)
	groupRoute.POST("", transp.WSCreateCategory)
}

//RouterProduct Method rest for products
func RouterProduct(useCase ProductUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseProduct(useCase)

	groupRoute := e.Group("/products")
	groupRoute.GET("/all", transp.WSViewProducts)
	groupRoute.GET("", transp.WSViewManyProducts)
	groupRoute.GET("/:id", transp.WSViewOneProduct)
	groupRoute.POST("/bulk", transp.WSCreateProducts)
	groupRoute.POST("", transp.WSCreateProduct)

}

//RouterInventory Method rest for inventory
func RouterInventory(useCase InventoryUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseInventory(useCase)

	groupRoute := e.Group("/inventories")
	groupRoute.POST("/bulk", transp.WSCreateInventories)
}

//RouterSupplier Method rest for inventory
func RouterSupplier(useCase SupplierUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseSupplier(useCase)

	groupRoute := e.Group("/suppliers")
	groupRoute.POST("/bulk", transp.WSCreateSuppliers)
}

//RouterPurchase Method rest for inventory
func RouterPurchase(useCase PurchaseUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCasePurchase(useCase)

	groupRoute := e.Group("/purchases")
	groupRoute.POST("/bulk", transp.WSCreatePurchases)
	groupRoute.POST("", transp.WSCreatePurchase)
}

//RouterCost Method rest for cost
func RouterCost(useCase CostUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseCost(useCase)

	groupRoute := e.Group("/costs")
	groupRoute.POST("/bulk", transp.WSCreateCosts)
	groupRoute.GET("/last", transp.WSGetLastCost)
}

//RouterPriceList Method rest for price list
func RouterPriceList(useCase PriceListUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCasePriceList(useCase)

	groupRoute := e.Group("/pricelists")
	groupRoute.POST("/bulk", transp.WSCreatePriceLists)
	groupRoute.GET("/last", transp.WSCreatePriceList)
}

//RouterSale Method rest for sale
func RouterSale(useCase SaleUseCase, e *echo.Echo) {

	transp := NewInterfaceUseCaseSale(useCase)

	groupRoute := e.Group("/sales")
	groupRoute.POST("/bulk", transp.WSCreateSales)
	groupRoute.POST("", transp.WSCreateSale)
}
