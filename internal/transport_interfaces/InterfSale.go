package transport

import (
	"net/http"

	"github.com/labstack/echo/v4"
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
	"gitlab.com/escaparate.com/escaparate_shared/errors"
)

//SaleUseCase methods CRUD that need store or infraestructure
type SaleUseCase interface {
	CreateSales() error
	CreateSale(Sale entity.Sale) error
	//FindSales() (*[]entity.Sale, error)
	//FindSale(Sale entity.Sale) (*entity.Sale, error)
}

//SaleMethodUC struct that contain the interface whit methods CRUD for ocupping in the store
type SaleMethodUC struct {
	UseCase SaleUseCase
}

//NewInterfaceUseCaseSale return an interface salut of use case
func NewInterfaceUseCaseSale(SaleUseCase SaleUseCase) SaleMethodUC {
	return SaleMethodUC{
		UseCase: SaleUseCase,
	}
}

//WSCreateSales Web service many Sales in a database
func (sal SaleMethodUC) WSCreateSales(c echo.Context) error {

	err := sal.UseCase.CreateSales()
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all Sales")
	}

	return c.JSON(http.StatusCreated, "Sales create succesfuly")
}

//WSCreateSale Web service a Sale in a database
func (sal SaleMethodUC) WSCreateSale(c echo.Context) error {

	SaleBody := entity.Sale{}

	if err := c.Bind(&SaleBody); err != nil {
		return errors.WrapError(err, 400, "Sale body request not found")
	}

	err := sal.UseCase.CreateSale(SaleBody)
	if err != nil {
		return errors.WrapError(err, 500, "Ocurred an error to insert all Sales")
	}

	return c.JSON(http.StatusCreated, "Sales create succesfuly")
}
