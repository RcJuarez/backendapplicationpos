package usecase

import (
	"errors"
	"time"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//PurchaseStore methods to access to databaase
type PurchaseStore interface {
	InsertPurchases() error
	InsertPurchase(purchase entity.Purchase) error

	SelectPurchaseBySupplierAndProductAndCost(purchase entity.Purchase) (entity.Purchase, error)

	//Store costs
	InsertCost(cost entity.Cost) error
	SelectCostBySupplierAndProductAndCost(cost entity.Cost) (entity.Cost, error)
	UpdateCostDateCreate(cost entity.Cost) error

	//SelectPurchases() (*[]entity.Purchase, error)
	//SelectPurchase(purchase entity.Purchase) (*entity.Purchase, error)
}

//AccesInterfacePurchase struct to access the interface method
type AccesInterfacePurchase struct {
	Interface PurchaseStore
	//InterfaceCost CostStore
}

//NewAccessInterfacePurchase return a new interface of purchase store
func NewAccessInterfacePurchase(store PurchaseStore) AccesInterfacePurchase {
	//return AccesInterfacePurchase{Interface: store, InterfaceCost: storeCost}
	return AccesInterfacePurchase{Interface: store}
}

//CreatePurchases utilize the store to create many purchases in a database
func (interf AccesInterfacePurchase) CreatePurchases() error {
	return interf.Interface.InsertPurchases()
}

//CreatePurchase utilize the sotore to create a purchase in a database
func (interf AccesInterfacePurchase) CreatePurchase(data entity.Purchase) error {

	//Search a purchase with the same id product, id supplier, value cost and datecreated
	purchaseFound, err := interf.Interface.SelectPurchaseBySupplierAndProductAndCost(data)
	if err != nil {
		return err
	}
	//if before purchase not found, it will create a purchase
	if purchaseFound == (entity.Purchase{}) {
		err := interf.Interface.InsertPurchase(data)
		if err != nil {
			return errors.New("Error creating a purchase: " + err.Error())
		}
		//Update or insert a cost depends if exist
		cost := entity.Cost{
			CreatedAt:  time.Now(),
			SupplierID: data.SupplierID,
			ProductID:  data.ProductID,
			ValueCost:  data.ValueCost,
		}
		return interf.CreateOrModifyCost(cost)
	}
	return errors.New("There is already a purchase with the same supplier and the same cost")
}

//CreateOrModifyCost find a cost and if find it, update it in the database else insert a new cost
func (interf AccesInterfacePurchase) CreateOrModifyCost(cost entity.Cost) error {
	//Search cost with same supplier, product and cost
	costFound, err := interf.Interface.SelectCostBySupplierAndProductAndCost(cost)
	if err != nil {
		return errors.New("Error finding cost in the database")
	}
	//If there aren't costs, it going to save in a database
	if costFound == (entity.Cost{}) {
		return interf.Interface.InsertCost(cost)

	}
	//will compare the date and will insert the recently date
	if cost.CreatedAt.Before(costFound.CreatedAt) {
		//update it with most recenct date
		return interf.Interface.UpdateCostDateCreate(cost)
	}
	return err
}
