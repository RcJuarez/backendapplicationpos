package usecase

import (
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//PriceListStore methods to access to databaase
type PriceListStore interface {
	InsertPriceLists(priceList []entity.PriceList) error
	InsertPriceList(purchase entity.PriceList) error
	ReadPriceListsFromCSV() ([]entity.PriceList, error)

	//SelectPriceLists() (*[]entity.PriceList, error)
	//SelectPriceList(purchase entity.PriceList) (*entity.PriceList, error)
}

//AccesInterfacePriceList struct to access the interface method
type AccesInterfacePriceList struct {
	Interface PriceListStore
}

//NewAccessInterfacePriceList return a new interface of purchase store
func NewAccessInterfacePriceList(store PriceListStore) AccesInterfacePriceList {
	//return AccesInterfacePriceList{Interface: store, InterfaceCost: storeCost}
	return AccesInterfacePriceList{Interface: store}
}

//CreatePriceLists utilize the store to create many purchases in a database
func (interf AccesInterfacePriceList) CreatePriceLists() error {
	listPrices, err := interf.Interface.ReadPriceListsFromCSV()
	if err != nil {
		return err
	}
	return interf.Interface.InsertPriceLists(listPrices)
}

//CreatePriceList utilize the sotore to create a purchase in a database
func (interf AccesInterfacePriceList) CreatePriceList(data entity.PriceList) error {
	return interf.Interface.InsertPriceList(data)
}
