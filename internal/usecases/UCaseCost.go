package usecase

import (
	"errors"
	"fmt"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//CostStore methods to access to databaase
type CostStore interface {
	ReadCostsFromCSV() ([]entity.Cost, error)
	InsertCost(cost entity.Cost) error
	SelectCostBySupplierAndProductAndCost(cost entity.Cost) (entity.Cost, error)
	UpdateCostDateCreate(cost entity.Cost) error
	SelectLastCost(cost entity.Cost) (entity.Cost, error)
}

//AccesInterfaceCost struct to access the interface method
type AccesInterfaceCost struct {
	Interface CostStore
}

//NewAccessInterfaceCost return a new interface of cost store
func NewAccessInterfaceCost(store CostStore) AccesInterfaceCost {
	return AccesInterfaceCost{Interface: store}
}

//CreateCosts utilize the store to create a new cost in a database by reading a file CSV
func (interf AccesInterfaceCost) CreateCosts() error {
	fmt.Println("creando costos")
	costs, err := interf.Interface.ReadCostsFromCSV()
	if err != nil {
		return err
	}
	fmt.Println("Numero de costos: ", len(costs))
	if len(costs) == 0 {
		return errors.New("File Cost Empty")
	}

	for _, cost := range costs {
		//supplier id or product id can't be empty
		if cost.SupplierID == 0 || cost.ProductID == "" {
			return errors.New("Supplier id or product id can't be empty")
		}
		err = interf.CreateOrModifyCost(cost)
		if err != nil {
			return err
		}
	}
	return nil
}

//CreateOrModifyCost find a cost and if find it, update it in the database else insert a new cost
func (interf AccesInterfaceCost) CreateOrModifyCost(cost entity.Cost) error {
	//Search cost with same supplier, product and cost
	costFound, err := interf.Interface.SelectCostBySupplierAndProductAndCost(cost)
	if err != nil {
		return errors.New("Error finding cost in the database")
	}
	//If there aren't costs, it going to save in a database
	if costFound == (entity.Cost{}) {
		return interf.Interface.InsertCost(cost)

	}
	//will compare the date and will insert the recently date
	if cost.CreatedAt.Before(costFound.CreatedAt) {
		//update it with most recenct date
		return interf.Interface.UpdateCostDateCreate(cost)
	}
	return err
}

//GetLastCost return the last cost ordered by date by passing the supplier id and product id
func (interf AccesInterfaceCost) GetLastCost(cost entity.Cost) (entity.Cost, error) {
	costFound, err := interf.Interface.SelectLastCost(cost)
	if err != nil {
		return costFound, errors.New("Error to get last cost: " + err.Error())
	}
	return costFound, err
}
