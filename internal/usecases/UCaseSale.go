package usecase

import (
	"errors"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//SaleStore methods to access to databaase
type SaleStore interface {
	ReadSalesFromCSV() ([]entity.Sale, error)
	InsertSales(sales []entity.Sale) error
	InsertSale(sale entity.Sale) error

	SelectSameSale(sale entity.Sale) (entity.Sale, error)
}

//AccesInterfaceSale struct to access the interface method
type AccesInterfaceSale struct {
	Interface SaleStore
	//InterfaceSale SaleStore
}

//NewAccessInterfaceSale return a new interface of sale store
func NewAccessInterfaceSale(store SaleStore) AccesInterfaceSale {
	//return AccesInterfaceSale{Interface: store, InterfaceSale: storeSale}
	return AccesInterfaceSale{Interface: store}
}

//CreateSales utilize the store to create many sales in a database
func (interf AccesInterfaceSale) CreateSales() error {
	data, err := interf.Interface.ReadSalesFromCSV()
	if err != nil {
		return err
	}
	return interf.Interface.InsertSales(data)
}

//CreateSale utilize the sotore to create a sale in a database
func (interf AccesInterfaceSale) CreateSale(data entity.Sale) error {

	//Search a sale with the same id product, id supplier, value cost and datecreated
	saleFound, err := interf.Interface.SelectSameSale(data)
	if err != nil {
		return err
	}
	//if before sale not found, it will create a sale
	if saleFound == (entity.Sale{}) {
		err := interf.Interface.InsertSale(data)
		if err != nil {
			return errors.New("Error creating a sale: " + err.Error())
		}
	}
	return errors.New("There is already a sale with the same supplier and the same cost")
}
