package usecase

import (
	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//CategoryStore methods to access to databaase
type CategoryStore interface {
	InsertCategory(category entity.Category) (*entity.Category, error)
	SelectCategories() (*[]entity.Category, error)
}

//AccesInterfaceCategory struct to access the interface method
type AccesInterfaceCategory struct {
	Interface CategoryStore
}

//NewAccessInterfaceCategory return a new interface of category store
func NewAccessInterfaceCategory(store CategoryStore) AccesInterfaceCategory {
	return AccesInterfaceCategory{Interface: store}
}

//CreateCategory  utilize the store to create a new product in a database
func (u AccesInterfaceCategory) CreateCategory(category entity.Category) (*entity.Category, error) {
	categoryCreated, err := u.Interface.InsertCategory(category)
	if err != nil {
		return nil, err
	}
	return categoryCreated, nil
}

//FindCategories search all categories that exist on the database
func (u AccesInterfaceCategory) FindCategories() (*[]entity.Category, error) {
	categories, err := u.Interface.SelectCategories()
	if err != nil {
		return nil, err
	}
	return categories, nil
}
