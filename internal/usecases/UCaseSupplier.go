package usecase

//SupplierStore methods to access to databaase
type SupplierStore interface {
	InsertSuppliers() error
	//InsertSupplier(Supplier entity.Supplier) (*entity.Supplier, error)
	//SelectSuppliers() (*[]entity.Supplier, error)
	//SelectSupplier(Supplier entity.Supplier) (*entity.Supplier, error)
}

//AccesInterfaceSupplier struct to access the interface method
type AccesInterfaceSupplier struct {
	Interface SupplierStore
}

//NewAccessInterfaceSupplier return a new interface of Supplier store
func NewAccessInterfaceSupplier(store SupplierStore) AccesInterfaceSupplier {
	return AccesInterfaceSupplier{Interface: store}
}

//CreateSuppliers utilize the store to create many Suppliers in a database
func (interf AccesInterfaceSupplier) CreateSuppliers() error {
	return interf.Interface.InsertSuppliers()
}
