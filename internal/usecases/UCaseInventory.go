package usecase

//InventoryStore methods to access to databaase
type InventoryStore interface {
	InsertInventories() error
}

//AccesInterfaceInventory struct to access the interface method
type AccesInterfaceInventory struct {
	Interface InventoryStore
}

//NewAccessInterfaceInventory return a new interface of inventory store
func NewAccessInterfaceInventory(store InventoryStore) AccesInterfaceInventory {
	return AccesInterfaceInventory{Interface: store}
}

//CreateInventories utilize the store to create many products in a database
func (interf AccesInterfaceInventory) CreateInventories() error {
	return interf.Interface.InsertInventories()
}
