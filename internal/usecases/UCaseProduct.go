package usecase

import (
	"errors"

	entity "gitlab.com/escaparate.com/backendapplicationpos/internal/entity_domain"
)

//ProductStore methods to access to databaase
type ProductStore interface {
	InsertProducts() error
	InsertProduct(product entity.Product) (*entity.Product, error)
	SelectProducts() (*[]entity.Product, error)
	SelectProduct(product entity.Product) (*entity.Product, error)
	SelectProductByCode(searched string) ([]entity.Product, error)
}

//AccesInterfaceProduct struct to access the interface method
type AccesInterfaceProduct struct {
	Interface ProductStore
}

//NewAccessInterfaceProduct return a new interface of product store
func NewAccessInterfaceProduct(store ProductStore) AccesInterfaceProduct {
	return AccesInterfaceProduct{Interface: store}
}

//CreateProducts utilize the store to create many products in a database
func (interf AccesInterfaceProduct) CreateProducts() error {
	return interf.Interface.InsertProducts()
}

//CreateProduct utilize the store to create a new product in a database
func (interf AccesInterfaceProduct) CreateProduct(product entity.Product) (*entity.Product, error) {

	if product.CategoryID == 0 {
		return nil, errors.New("Category Id can't be empty")
	}

	productCreated, err := interf.Interface.InsertProduct(product)
	if err != nil {
		return nil, err
	}
	return productCreated, nil
}

//FindProducts find all productos on a database
func (interf AccesInterfaceProduct) FindProducts() (*[]entity.Product, error) {
	prods, err := interf.Interface.SelectProducts()
	if err != nil {
		return nil, err
	}
	return prods, nil
}

//FindProduct fin one product with the code passed in the parameter
func (interf AccesInterfaceProduct) FindProduct(product entity.Product) (*entity.Product, error) {
	prod, err := interf.Interface.SelectProduct(product)
	return prod, err
}

//SelectProductByCode find a product with parameter passed
func (interf AccesInterfaceProduct) SelectProductByCode(searched string) ([]entity.Product, error) {
	prods, err := interf.Interface.SelectProductByCode(searched)
	return prods, err
}
