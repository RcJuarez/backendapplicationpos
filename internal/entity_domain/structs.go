package entity

import (
	"time"

	"github.com/jinzhu/gorm"
)

type (

	//Category belongs a product
	Category struct {
		ID   int    `gorm:"PRIMARY_KEY; type: serial;"`
		Name string `gorm:"type:varchar(20);no null; unique"`
	}

	//Product is a struct that save all characterist products, one product belongs to Category
	Product struct {
		Code        string `gorm:"PRIMARY_KEY; not null;"`
		BarCode     string
		Name        string
		Description string
		CommonNames string
		CategoryID  int
		Category    Category
		Inventory   Inventory
	}

	//Supplier struct to save information about supliers
	Supplier struct {
		ID       int    `gorm:"PRIMARY_KEY; type: serial;"`
		Fullname string `gorm:"type:varchar(60)"`
		Addres   string `gorm:"type:varchar(100)"`
		Phone    string `gorm:"type:varchar(10)"`
		Email    string `gorm:"type:varchar(50)"`
	}

	/*
		//CostList Save statistics of a cost of a supplier
		CostList struct {
			ID          int `gorm:"PRIMARY_KEY; type: serial;"`
			AverageCost float64
			MinCost     float64
			MaxCost     float64
			LastCost    float64
			SupplierID  int
			Supplier    Supplier
		}
	*/

	//Cost All cost of the cost product
	Cost struct {
		ID         int `gorm:"PRIMARY_KEY; type: serial;"`
		CreatedAt  time.Time
		SupplierID int
		Supplier   Supplier
		ProductID  string
		Product    Product `gorm:"association_foreignkey:Code"`
		ValueCost  float64
	}

	//Customer Information about Customer
	Customer struct {
		ID             int    `gorm:"PRIMARY_KEY; type: serial;"`
		Name           string `gorm:"type:varchar(60)"`
		FirstLastName  string `gorm:"type:varchar(30)"`
		SecondLastName string `gorm:"type:varchar(30)"`
		Address        string `gorm:"type:varchar(50)"`
		Email          string `gorm:"type:varchar(50)"`
		Phone          string `gorm:"type:varchar(10)"`
	}

	//Inventory contains quantity of each product
	Inventory struct {
		gorm.Model
		//ID            int `gorm:"PRIMARY_KEY; type: serial;"`
		ProductID string
		//Product       Product `gorm:"association_foreignkey:Code"`
		Unit          string `gorm:"type:varchar(5)"`
		InitInventory bool
		InitQuantity  int
		Comments      string `gorm:"type:varchar(100)"`
	}

	//NameList is the list of echa price product
	NameList struct {
		gorm.Model
		Name         string `gorm:"type:varchar(25)"`
		DiscountRate int
		Default      bool
		Priority     int
	}

	//PriceList join the struct Price whit the struct Product, contains price of each product
	PriceList struct {
		gorm.Model
		NameListID int
		NameList   NameList
		ProductID  string
		Product    Product `gorm:"association_foreignkey:Code"`
		Price      float64
	}

	//Purchase Contains all purchase of a buiness
	Purchase struct {
		ID         int `gorm:"PRIMARY_KEY; type: serial;"`
		CreatedAt  time.Time
		UpdatedAt  time.Time
		DeletedAt  *time.Time `sql:"index"`
		SupplierID int
		Supplier   Supplier
		ProductID  string
		Product    Product `gorm:"association_foreignkey:Code"`
		Quantity   float64
		ValueCost  float64
		Ammount    float64
		Comments   string
	}

	//Seller person who do the purchase and sales or who work in the business
	Seller struct {
		ID       int    `gorm:"PRIMARY_KEY; type: serial;"`
		FullName string `gorm:"type:varchar(60)"`
		Email    string `gorm:"type:varchar(50)"`
		Phone    string `gorm:"type:varchar(10)"`
	}

	//Sale principal operation of a business
	Sale struct {
		ID          int `gorm:"primary_key"`
		CreatedAt   time.Time
		UpdatedAt   time.Time
		DeletedAt   *time.Time `sql:"index"`
		ProductID   string
		Product     Product `gorm:"association_foreignkey:Code"`
		Quantity    float64
		PriceListID int
		PriceList   PriceList
		Price       float64
		Ammount     float64
		SellerID    int
		Seller      Seller
		CustomerID  int
		Customer    Customer
		Comments    string
	}
)
