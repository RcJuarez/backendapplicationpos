package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	store "gitlab.com/escaparate.com/backendapplicationpos/internal/store_infraestructure"
	transport "gitlab.com/escaparate.com/backendapplicationpos/internal/transport_interfaces"
	usecase "gitlab.com/escaparate.com/backendapplicationpos/internal/usecases"
	"gitlab.com/escaparate.com/escaparate_shared/config"
	"gitlab.com/escaparate.com/escaparate_shared/errors/http_response"
)

func main() {

	configuracionGlobal := config.GetConfiguracion()

	// Init Database
	dataBase, err := store.InitDatabase(*configuracionGlobal.Postgres)
	if err != nil {
		log.Fatal("Error to connect PostgresSQL database", err.Error())
	}

	//Creas las tablas iniciando el proyecto.
	err = store.InitTables(dataBase)
	if err != nil {
		log.Fatal("Error to create database tables", err.Error())
	}

	//Init framework echo
	e := echo.New()

	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.CORS())
	e.Use(middleware.Recover())
	e.HTTPErrorHandler = http_response.HttpErrorHandler

	//Route about categories
	transport.RouterCategory(usecase.NewAccessInterfaceCategory(dataBase), e)

	//Route about products
	transport.RouterProduct(usecase.NewAccessInterfaceProduct(dataBase), e)

	//Route about inventories
	transport.RouterInventory(usecase.NewAccessInterfaceInventory(dataBase), e)

	//Route about suppliers
	transport.RouterSupplier(usecase.NewAccessInterfaceSupplier(dataBase), e)

	//Route about purchases
	transport.RouterPurchase(usecase.NewAccessInterfacePurchase(dataBase), e)

	//Route about costs
	transport.RouterCost(usecase.NewAccessInterfaceCost(dataBase), e)

	//Route about List price
	transport.RouterPriceList(usecase.NewAccessInterfacePriceList(dataBase), e)

	//Route about Sale
	transport.RouterSale(usecase.NewAccessInterfaceSale(dataBase), e)

	//-------------------------------------------------
	// Se inicializa el servidor
	//-------------------------------------------------
	go func() {
		fmt.Println("Init server on: ", configuracionGlobal.Default.Puerto)
		if err := e.Start(configuracionGlobal.Default.Puerto); err != nil {
			e.Logger.Info("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

}
